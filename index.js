var express = require('express');
var ccf 	= require('calcolo-codice-fiscale');
var urlParse= require('url-parse');

var app 	= express();

app.get('/', function(req, res){
	res.end('Welcome, try: ' + "\r\n" + 'https://tools.docu-service.it/calcola/?nome=Mario&cognome=Rossi&gender=M&day=28&month=01&year=1979&city=Torino&state=TO' + " OR \r\nhttps://tools.docu-service.it/verifica/?cf=RSSMRA76P22L219H");
});

app.get('/calcola/', function(req, res){
	
	var parsed_url = urlParse(req.url, true);
	var query_obj  = parsed_url.query;

	var cf = ccf.CalcolaCodiceFiscale.compute({
	    name: 					query_obj.nome,
	    surname: 				query_obj.cognome,
	    gender: 				query_obj.gender,
	    day: 					query_obj.day,
	    month: 					query_obj.month,
	    year: 					query_obj.year,
	    birthplace: 			decodeURIComponent(query_obj.city), 
	    birthplace_provincia: 	decodeURIComponent(query_obj.state)
	});
	
	res.end(JSON.stringify({
		name: 					query_obj.nome,
	    surname: 				query_obj.cognome,
	    gender: 				query_obj.gender,
	    day: 					query_obj.day,
	    month: 					query_obj.month,
	    year: 					query_obj.year,
	    birthplace: 			decodeURIComponent(query_obj.city), 
	    birthplace_provincia: 	decodeURIComponent(query_obj.state),
	    codice_fiscale: 		cf
	}));

	//res.end('http://tools.seo-business.it/calcola/?nome=Mario&cognome=Rossi&gender=M&day=28&month=01&year=1979&city=Torino&state=Torino' + JSON.stringify(parsed_url.query));
});

app.get('/verifica/', function(req, res){
	
	var parsed_url = urlParse(req.url, true);
	var query_obj  = parsed_url.query;

	var isValid = ccf.CalcolaCodiceFiscale.check(query_obj.cf);
	
	res.end(JSON.stringify({
		response: isValid,
	    codice_fiscale: query_obj.cf
	}));
});

app.listen('80');

//cf?nome=Mario&cognome=Rossi&gg=28&m=01&y=1979&placeofbirth=Torino&stateofbirth=Torino